# Kudos Server

**Kudos Server** a fast Voting System 

See in action: https://sebastian.korotkiewicz.eu/techlog/activate-tap-to-click-on-touchpad/

## Getting started

1. PHP 5.3.x is required
2. Install Klein.php using Composer
3. Setup [URL rewriting](https://gitlab.itunix.eu/skorotkiewicz/kudos-server/blob/master/public/.htaccess) so that all requests are handled by **index.php**
4. Rename config_example.ini to config.ini set your admin password and allowed sitemap for your blog

## Composer Installation

1. Get [Composer](http://getcomposer.org/)
2. Require Klein with `php composer.phar install`
3. Update datebase (update.php) with allowed blog posts
## Set permission db for web server

```
$ cd /path/to/kudos
$ chgrp www-data data/
$ chgrp www-data data/kudos.db
$ chmod g+w data/
$ chmod g+w data/allowed.txt
```
