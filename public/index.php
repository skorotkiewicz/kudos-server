<?php

header("Access-Control-Allow-Origin: *");

require '../vendor/autoload.php';
$klein = new \Klein\Klein();

$klein->respond(function ($request, $response, $service, $app) use ($klein) {

    $app->register('db', function() {
        return new PDO('sqlite:'.dirname(__FILE__).'/../data/kudos.db');
    });

    // bulid the datebase if not exist
    if (filesize(dirname(__FILE__).'/../data/kudos.db') == 0) {
        $app->db->query("CREATE TABLE IF NOT EXISTS kudosplease (
                    id INTEGER PRIMARY KEY,
                    mypath TEXT,
                    kudos TEXT
                )");
        $app->db->query("INSERT INTO kudosplease (mypath, kudos) VALUES ('1', '1')");
    }
});

$klein->respond('GET', '/', function ($request, $response, $service) {
    //$service->render('../views/index.phtml');
    return $response->code(200)->append('-+-');
});

$klein->respond('GET', '/api/kudo/[:url]', function ($request, $response, $service, $app) {
    return getKudo($request->url, $response, $service, $app);
});

$klein->respond('POST', '/api/kudo/[:action]/[:url]', function ($request, $response, $service, $app) {

    if (!isset($request->url) && $request->url == "" )  { return $response->code(422)->append('No valid field'); }
    if ($request->action != "add" &&  $request->action != "remove")  { return $response->code(422)->append('No valid field'); }    
    $url = $request->url;

    if (!allowedArticles($url)) { return $response->code(401)->append('No valid field'); }

    $stmt = $app->db->prepare("SELECT * FROM kudosplease WHERE mypath = :mypath");
    $stmt->execute(array(':mypath'=>$url));
    $result = $stmt->fetch();

    if (isset($result) && $result > 0) {

        if ($request->action == 'add') {
            $stmt = $app->db->prepare("UPDATE kudosplease SET kudos = kudos + 1 WHERE mypath = :mypath");
        }
        if ($request->action == 'remove') {
            $stmt = $app->db->prepare("UPDATE kudosplease SET kudos = kudos - 1 WHERE mypath = :mypath");
        }
        $stmt->execute(array(':mypath'=>$url));

        getKudo($url, $response, $service, $app);

    } else {

        $stmt = $app->db->prepare("INSERT INTO kudosplease (mypath, kudos) VALUES (?, ?)");
        $stmt->execute(array($url, 0));

        return $response->code(201)->append('0');

    }

});

$klein->respond('GET', '/api/update/[:key]', function ($request, $response, $service, $app) {

    //https://example.com/api/update/[password]
    $ini = parse_ini_file(dirname(__FILE__).'/../config.ini');

    if (isset($request->key) && $request->key == $ini['update_password'] )  { 

        unlink(dirname(__FILE__).'/../data/allowed.txt');

        $input = file_get_contents($ini['path_sitemap'], true);
        $data = iconv(mb_detect_encoding($data, mb_detect_order(), true), "UTF-8", $input);
        $xml = simplexml_load_string($data);

        // save all posts to file with md5 hash
        $current = file_get_contents(dirname(__FILE__).'/../data/allowed.txt');
        foreach ($xml->url as $url) {
            $current .= md5($url->loc) ."\n";
        }
        file_put_contents(dirname(__FILE__).'/../data/allowed.txt', $current);

        return $response->code(201)->append('Done'); 

    } else {
        return $response->code(422)->append('No valid field'); 
    }

});

$klein->dispatch();

function getKudo($url, $response, $service, $app) {

    $stmt = $app->db->prepare("SELECT * FROM kudosplease WHERE mypath = :mypath");
    $stmt->execute(array(':mypath'=>$url));
    $result = $stmt->fetch();

    if (isset($result) && $result > 0) {

        return $response->code(200)->append($result['kudos']);

    } else {

        if (!allowedArticles($url)) { return $response->code(401)->append('No valid field'); }

        $stmt = $app->db->prepare("INSERT INTO kudosplease (mypath, kudos) VALUES (?, ?)");
        $stmt->execute(array($url, 0));

        return $response->code(201)->append('0');

    }

}

function allowedArticles($url){

    // check if md5 hash is in file
    if( strpos(file_get_contents(dirname(__FILE__).'/../data/allowed.txt'),$url) !== false) {
        return true;
    } else {
        return false;
    }

}


?>



