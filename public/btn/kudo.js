$(function()
{
    $("figure.kudoable").kudoable();
    
    var uid = $("figure.kudoable").data("uid");

    // check to see if user has already kudod
    if($.cookie(uid) == 'true') {
        $("figure.kudoable").removeClass("animate").addClass("complete");
    }	

    $.get( "https://kudos.itunix.eu/api/kudo/" + uid, function( msg ) {
        $(".num").html(msg);
    });

    $("figure.kudoable").on("kudo:added", function(event) {
        $.post("https://kudos.itunix.eu/api/kudo/add/" + uid);
        $.cookie(uid, 'true', { expires: 7 });
    });

    $("figure.kudoable").on("kudo:removed", function(event) {
        $.post("https://kudos.itunix.eu/api/kudo/remove/" + uid);
        $.removeCookie(uid);
    });
});